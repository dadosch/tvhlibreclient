package org.tvheadend.tvhlibreclient.ui.features.information;

import android.os.Bundle;

import org.tvheadend.tvhlibreclient.R;
import org.tvheadend.tvhlibreclient.ui.base.BaseActivity;
import org.tvheadend.tvhlibreclient.util.MiscUtils;

import androidx.appcompat.widget.Toolbar;

public class WebViewActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(MiscUtils.getThemeId(this));
        super.onCreate(savedInstanceState);
        setContentView(R.layout.misc_content_activity);
        MiscUtils.setLanguage(this);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        if (savedInstanceState == null) {
            WebViewFragment fragment = new WebViewFragment();
            fragment.setArguments(getIntent().getExtras());
            getSupportFragmentManager().beginTransaction().replace(R.id.main, fragment).commit();
        }
    }
}
