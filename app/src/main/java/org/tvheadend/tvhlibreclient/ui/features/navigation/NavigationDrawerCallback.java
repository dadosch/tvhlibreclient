package org.tvheadend.tvhlibreclient.ui.features.navigation;

public interface NavigationDrawerCallback {

    void onNavigationMenuSelected(int id);
}
