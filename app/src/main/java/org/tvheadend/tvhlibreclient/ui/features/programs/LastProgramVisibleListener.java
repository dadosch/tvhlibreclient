package org.tvheadend.tvhlibreclient.ui.features.programs;

interface LastProgramVisibleListener {
    void onLastProgramVisible(int position);
}
