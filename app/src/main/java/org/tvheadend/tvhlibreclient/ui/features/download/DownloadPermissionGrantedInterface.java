package org.tvheadend.tvhlibreclient.ui.features.download;

public interface DownloadPermissionGrantedInterface {
    void downloadRecording();
}
