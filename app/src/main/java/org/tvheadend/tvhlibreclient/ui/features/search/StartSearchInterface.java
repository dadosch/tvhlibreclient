package org.tvheadend.tvhlibreclient.ui.features.search;

public interface StartSearchInterface {
    void startSearch();
}
