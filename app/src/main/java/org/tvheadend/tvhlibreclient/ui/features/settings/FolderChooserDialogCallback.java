package org.tvheadend.tvhlibreclient.ui.features.settings;

import java.io.File;

interface FolderChooserDialogCallback {
    void onFolderSelected(File folder);
}
