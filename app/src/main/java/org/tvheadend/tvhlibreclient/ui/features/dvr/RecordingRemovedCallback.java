package org.tvheadend.tvhlibreclient.ui.features.dvr;

public interface RecordingRemovedCallback {
    void onRecordingRemoved();
}
