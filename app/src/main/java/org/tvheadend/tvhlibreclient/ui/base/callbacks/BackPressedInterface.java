package org.tvheadend.tvhlibreclient.ui.base.callbacks;

public interface BackPressedInterface {
    void onBackPressed();
}
