package org.tvheadend.tvhlibreclient.ui.features.search;

public interface SearchRequestInterface {

    void onSearchRequested(String query);

    boolean onSearchResultsCleared();

    String getQueryHint();
}
