package org.tvheadend.tvhlibreclient.ui.features.dvr;

import org.tvheadend.tvhlibreclient.domain.entity.Channel;

public interface RecordingConfigSelectedListener {

    void onChannelSelected(Channel channel);
    
    void onProfileSelected(int which);

    void onPrioritySelected(int which);

    void onDaysSelected(int selectedDays);
}
