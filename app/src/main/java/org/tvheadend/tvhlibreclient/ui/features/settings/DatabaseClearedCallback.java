package org.tvheadend.tvhlibreclient.ui.features.settings;

public interface DatabaseClearedCallback {
    void onDatabaseCleared();
}
