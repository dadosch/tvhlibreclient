package org.tvheadend.tvhlibreclient.util;

public class Constants {

    // Product id for the in-app billing item to unlock the application
    public static final String UNLOCKER = "unlocker";

}
