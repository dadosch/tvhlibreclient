package org.tvheadend.tvhlibreclient.domain.entity

interface ProgramInterface {
    var eventId: Int
    var title: String?
    var channelId: Int
    var start: Long
    var stop: Long
}