package org.tvheadend.tvhlibreclient.di;

import android.content.Context;
import android.content.SharedPreferences;

import org.tvheadend.tvhlibreclient.MainApplication;
import org.tvheadend.tvhlibreclient.data.repository.AppRepository;
import org.tvheadend.tvhlibreclient.data.service.HtspIntentService;
import org.tvheadend.tvhlibreclient.data.service.HtspService;
import org.tvheadend.tvhlibreclient.data.service.htsp.HtspConnection;
import org.tvheadend.tvhlibreclient.di.modules.MainApplicationModule;
import org.tvheadend.tvhlibreclient.di.modules.RepositoryModule;
import org.tvheadend.tvhlibreclient.di.modules.SharedPreferencesModule;
import org.tvheadend.tvhlibreclient.ui.base.BaseFragment;
import org.tvheadend.tvhlibreclient.ui.features.MainActivity;
import org.tvheadend.tvhlibreclient.ui.features.channels.BaseChannelViewModel;
import org.tvheadend.tvhlibreclient.ui.features.download.DownloadRecordingManager;
import org.tvheadend.tvhlibreclient.ui.features.dvr.recordings.RecordingViewModel;
import org.tvheadend.tvhlibreclient.ui.features.dvr.series_recordings.SeriesRecordingViewModel;
import org.tvheadend.tvhlibreclient.ui.features.dvr.timer_recordings.TimerRecordingViewModel;
import org.tvheadend.tvhlibreclient.ui.features.epg.EpgViewPagerFragment;
import org.tvheadend.tvhlibreclient.ui.features.playback.external.BasePlaybackActivity;
import org.tvheadend.tvhlibreclient.ui.features.programs.ProgramViewModel;
import org.tvheadend.tvhlibreclient.ui.features.settings.BasePreferenceFragment;
import org.tvheadend.tvhlibreclient.ui.features.settings.ConnectionViewModel;
import org.tvheadend.tvhlibreclient.ui.features.settings.SettingsConnectionBaseFragment;
import org.tvheadend.tvhlibreclient.ui.features.settings.SettingsListConnectionsFragment;
import org.tvheadend.tvhlibreclient.ui.features.startup.StartupFragment;
import org.tvheadend.tvhlibreclient.util.MigrateUtils;
import org.tvheadend.tvhlibreclient.util.menu.MenuUtils;

import javax.inject.Singleton;

import dagger.Component;

@Component(modules = {
        MainApplicationModule.class,
        SharedPreferencesModule.class,
        RepositoryModule.class})
@Singleton
public interface MainApplicationComponent {

    Context context();

    SharedPreferences sharedPreferences();

    AppRepository appRepository();

    void inject(MainApplication mainApplication);

    void inject(RecordingViewModel recordingViewModel);

    void inject(BaseFragment baseFragment);

    void inject(BasePreferenceFragment basePreferenceFragment);

    void inject(ProgramViewModel programViewModel);

    void inject(TimerRecordingViewModel timerRecordingViewModel);

    void inject(SeriesRecordingViewModel seriesRecordingViewModel);

    void inject(BasePlaybackActivity basePlayActivity);

    void inject(MenuUtils menuUtils);

    void inject(SettingsListConnectionsFragment settingsListConnectionsFragment);

    void inject(ConnectionViewModel connectionViewModel);

    void inject(MigrateUtils migrateUtils);

    void inject(DownloadRecordingManager downloadRecordingManager);

    void inject(MainActivity mainActivity);

    void inject(StartupFragment startupFragment);

    void inject(EpgViewPagerFragment epgViewPagerFragment);

    void inject(SettingsConnectionBaseFragment settingsConnectionBaseFragment);

    void inject(BaseChannelViewModel baseChannelViewModel);

    void inject(HtspService htspService);

    void inject(HtspConnection htspConnection);

    void inject(HtspIntentService htspIntentService);
}
