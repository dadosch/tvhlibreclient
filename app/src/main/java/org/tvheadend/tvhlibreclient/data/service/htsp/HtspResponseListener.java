package org.tvheadend.tvhlibreclient.data.service.htsp;

public interface HtspResponseListener {

    void handleResponse(HtspMessage response);
}