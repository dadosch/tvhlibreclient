package org.tvheadend.tvhlibreclient.data.worker

import android.content.Context
import android.content.Intent
import androidx.work.ListenableWorker
import androidx.work.Worker
import androidx.work.WorkerParameters
import org.tvheadend.tvhlibreclient.data.service.HtspIntentService
import timber.log.Timber

class EpgDataUpdateWorker(context: Context, workerParams: WorkerParameters) : Worker(context, workerParams) {

    override fun doWork(): ListenableWorker.Result {
        Timber.d("Loading more event data from server")

        val intent = Intent()
        intent.action = "getMoreEvents"
        intent.putExtra("numFollowing", 250)
        HtspIntentService.enqueueWork(applicationContext, intent)
        return ListenableWorker.Result.success()
    }
}
