package org.tvheadend.tvhlibreclient.data.service.htsp;

public interface HtspMessageListener {

    void onMessage(HtspMessage response);
}
