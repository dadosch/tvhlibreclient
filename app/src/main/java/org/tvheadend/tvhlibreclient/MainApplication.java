package org.tvheadend.tvhlibreclient;

import android.content.SharedPreferences;

import com.facebook.stetho.Stetho;

import org.tvheadend.tvhlibreclient.di.DaggerMainApplicationComponent;
import org.tvheadend.tvhlibreclient.di.MainApplicationComponent;
import org.tvheadend.tvhlibreclient.di.modules.MainApplicationModule;
import org.tvheadend.tvhlibreclient.di.modules.RepositoryModule;
import org.tvheadend.tvhlibreclient.di.modules.SharedPreferencesModule;
import org.tvheadend.tvhlibreclient.util.MigrateUtils;
import org.tvheadend.tvhlibreclient.util.logging.DebugTree;
import org.tvheadend.tvhlibreclient.util.logging.FileLoggingTree;

import javax.inject.Inject;

import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.OnLifecycleEvent;
import androidx.lifecycle.ProcessLifecycleOwner;
import androidx.multidex.MultiDexApplication;
import timber.log.Timber;

// TODO use more livedata for connection and server status in a centralized viewmodel
// TODO move diffutils to background thread
// TODO Use paged loading
// TODO hide features menu when unlocker gets available later
// TODO show channel text if no icon is available
// TODO check where injected app context can be used

// TODO Move the variable programIdToBeEditedWhenBeingRecorded into the viewmodels
// TODO use a base viewmodel with generics

public class MainApplication extends MultiDexApplication implements LifecycleObserver {

    private static MainApplication instance;
    private static boolean activityVisible;
    @Inject
    protected SharedPreferences sharedPreferences;

    private static MainApplicationComponent component;


    public static synchronized MainApplication getInstance() {
        return instance;
    }

    public static MainApplicationComponent getComponent() {
        return component;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        ProcessLifecycleOwner.get().getLifecycle().addObserver(this);

        instance = this;
        // Create the component upon start of the app. This component
        // is used by all other classes to inject certain fields
        component = buildComponent();
        // Inject the shared preferences
        component.inject(this);

        // Enable stetho to enable accessing the database
        // and other resources via the chrome browser
        if (BuildConfig.DEBUG) {
            Stetho.initialize(Stetho.newInitializerBuilder(this)
                    .enableDumpapp(Stetho.defaultDumperPluginsProvider(this))
                    .enableWebKitInspector(Stetho.defaultInspectorModulesProvider(this))
                    .build());
        }

        initTimber();

        Timber.d("Application build time is " + BuildConfig.BUILD_TIME + ", git commit hash is " + BuildConfig.GIT_SHA);

        // Migrates existing connections from the old database to the new room database.
        // Migrates existing preferences or remove old ones before starting the actual application
        new MigrateUtils().doMigrate();
    }

    private MainApplicationComponent buildComponent() {
        return DaggerMainApplicationComponent.builder()
                .mainApplicationModule(new MainApplicationModule(this))
                .sharedPreferencesModule(new SharedPreferencesModule())
                .repositoryModule(new RepositoryModule(this))
                .build();
    }

    private void initTimber() {
        if (BuildConfig.DEBUG || BuildConfig.DEBUG_LOG) {
            Timber.plant(new DebugTree());
        }

        if (sharedPreferences.getBoolean("debug_mode_enabled",
                getResources().getBoolean(R.bool.pref_default_debug_mode_enabled))) {
            Timber.plant(new FileLoggingTree(getApplicationContext()));
        }
    }

    /**
     * Checks if the user has purchased the unlocker from the play store.
     * If yes the application is unlocked then all extra features are accessible.
     *
     * @return True if the application is unlocked otherwise false
     */
    public boolean isUnlocked() {
        return true;
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    public void onAppBackgrounded() {
        Timber.d("App is now in the background");
        activityVisible = false;
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    public void onAppForegrounded() {
        Timber.d("App is now in the foreground");
        activityVisible = true;
    }

    public static boolean isActivityVisible() {
        return activityVisible;
    }

}
